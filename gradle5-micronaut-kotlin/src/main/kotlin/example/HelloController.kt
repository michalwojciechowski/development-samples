/*
 * Copyright 2017 original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example

import io.micronaut.http.HttpRequest
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.reactivex.Single

/**
 * @author James Kleeh
 * @since 1.0
 */
@Controller("/")
class HelloController(
        val greetingService: GreetingService,
        @Client("http://localhost:8010") val httpClient: RxHttpClient) {

    @Get("/hello/{name}")
    fun hello(name: String): String {
        return greetingService.greet(name)
    }

    @Get("/ping")
    fun ping(): String {
        return "pong"
    }

    @Get("/reactive/{delayMillis}")
    fun reactive(delayMillis: Long): Single<String> {
        val req: HttpRequest<String> = HttpRequest.GET<String>("/delay/$delayMillis")
        return httpClient.retrieve(req).map { s -> "Reactive:$s" }.first("An error as occurred")
    }
}