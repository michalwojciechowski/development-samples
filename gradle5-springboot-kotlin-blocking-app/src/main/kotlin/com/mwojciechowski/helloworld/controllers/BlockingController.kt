package com.mwojciechowski.helloworld.controllers

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate

@RestController
class BlockingController constructor(builder: RestTemplateBuilder) {

    private var client: RestTemplate = builder.rootUri("http://localhost:8082").build()

    @GetMapping("/blocking/{delayMillis}")
    fun getBlocking(@PathVariable delayMillis: Long): String {
        return "Blocking: " + client.getForObject("/delay/$delayMillis", String::class.java)
    }
}








