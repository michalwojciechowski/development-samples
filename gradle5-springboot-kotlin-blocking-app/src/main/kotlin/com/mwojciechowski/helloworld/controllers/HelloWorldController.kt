package com.mwojciechowski.helloworld.controllers

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactive.flow.asFlow
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import java.time.Duration
import java.util.*

@RestController
class HelloWorldController {

    @GetMapping("/hello/{name}")
    suspend fun helloWorld(@PathVariable name: String): Response {
        return Response("Hello $name!")
    }

    data class Response(val message: String)
}