package com.mwojciechowski.helloworld.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.time.Duration

@RestController
class DelayController {

    @GetMapping("/delay/{delayMillis}")
    fun get(@PathVariable delayMillis: Long): Mono<String> {
        return Mono.just("OK").delayElement(Duration.ofMillis(delayMillis))
    }
}