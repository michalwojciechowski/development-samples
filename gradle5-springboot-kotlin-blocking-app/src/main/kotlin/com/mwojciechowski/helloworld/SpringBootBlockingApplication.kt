package com.mwojciechowski.helloworld

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringBootBlockingApplication

fun main(args: Array<String>) {
    runApplication<SpringBootBlockingApplication>(*args)
}
