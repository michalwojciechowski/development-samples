# Introduction

As of June 3rd 2019, GitLab does not fully support monorepo approach, a single repository for all your microservices. 
Despite the number of issues standing on the way to get it done, including:
* [Issue 28592](https://gitlab.com/gitlab-org/gitlab-ce/issues/28592)
* [Issue 18157](https://gitlab.com/gitlab-org/gitlab-ce/issues/18157)

there is a workaround available. The base limitation, which is unavailability to have a dedicated pipeline per 
directory/project, can be overcome by the following steps.

1. Explicitly include all the nested .gitlab-ci files in the main pipeline.
    ```
    include:
    - '/service1/.gitlab-ci.yml'
    - '/service2/.gitlab-ci.yml'
    ```
    
    and using distinguish job names per each service

    ```
    .service1:
      only:
        variables:
          - $MODIFIED_COMPONENT == "service1"
    
    build service1:
      stage: build
      extends: .service1
      script:
        - echo 'Building...'
    
    test service1:
      stage: test
      extends: .service1
      script:
        - echo 'Testing...'
    ```
    
    ```
    .service2:
      only:
        variables:
          - $MODIFIED_COMPONENT == "service2"
    build service2:
      stage: build
      extends: .service2
      script:
        - echo 'Building...'
    
    test service2:
      stage: test
      extends: .service2
      script:
        - echo 'Testing...'
    ```
    
    (see nested [.gitlab-ci.yml](./.gitlab-ci.yml) as an example)
    
    > Please keep in mind that if you do not need to have a separate & dedicated pipelines per service
    (because you are NOT using different building tools, languages or simply you are willing to share the build logic per each component), 
    you can do it in a much more simpler manner thanks to [issue 19232](https://gitlab.com/gitlab-org/gitlab-ce/issues/19232).

2. Detect which directory/service has been modified and trigger corresponding pipeline
    ```
    changed_folders=`git diff --dirstat=files,0 $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | sed 's/^[ 0-9.]\+% //g' | sed 's/\\///'`
    for service in $changed_folders
    do
            echo "About to trigger pipeline for $service"
            curl --request POST -F "variables[MODIFIED_COMPONENT]=${service}" -F token=${CI_JOB_TOKEN} -F ref=${CI_COMMIT_REF_NAME} https://gitlab.com/api/v4/projects/123456/trigger/pipeline
    done
    ```
    
    (see main [.gitlab-ci.yml](/.gitlab-ci.yml) as an example)

Depending on the owned Gitlab license, you will end up with a seamless flow of builds per each component in your monorepo.
![](overview.png)
