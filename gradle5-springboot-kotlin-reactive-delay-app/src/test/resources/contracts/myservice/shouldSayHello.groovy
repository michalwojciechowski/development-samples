package contracts.myservice

org.springframework.cloud.contract.spec.Contract.make {
    request {
        method 'GET'
        url "/hello/${value(consumer(regex('[0-9]{10}')))}"
        headers {
            contentType('text/plain')
        }
    }
    response {
        status OK()
        body(
                message: value(producer(regex('Hello [0-9]{10}!')))
        )
    }
}