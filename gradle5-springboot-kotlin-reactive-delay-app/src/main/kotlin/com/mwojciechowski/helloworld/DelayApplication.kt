package com.mwojciechowski.helloworld

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DelayApplication

fun main(args: Array<String>) {
    runApplication<DelayApplication>(*args)
}
