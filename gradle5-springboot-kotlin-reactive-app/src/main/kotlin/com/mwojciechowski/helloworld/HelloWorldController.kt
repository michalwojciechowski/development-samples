package com.mwojciechowski.helloworld

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactive.flow.asFlow
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import java.time.Duration
import java.util.*

@RestController
class HelloWorldController {

    @GetMapping("/hello/{name}")
    suspend fun helloWorld(@PathVariable name: String): Response {
        return Response("Hello $name!")
    }

    @GetMapping("/hello/suspendedFlow")
    suspend fun helloSuspendedFlow(): Flow<String> {
        val seq1 = Flux.just(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString())
        return seq1.asFlow()
    }

    @GetMapping("/hello/flow")
    fun helloFlow(): Flow<String> {
        val seq1 = Flux.just(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString())
        return seq1.asFlow()
    }

    @GetMapping("/hello/flux")
    fun helloFlux(): Flux<String> {
        val seq1 = Flux.just(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString())
        return seq1.delayElements(Duration.ofSeconds(1))
    }

    data class Response(val message: String)
}