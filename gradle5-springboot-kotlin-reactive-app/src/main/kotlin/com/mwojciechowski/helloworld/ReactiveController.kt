package com.mwojciechowski.helloworld

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

@RestController
class ReactiveController {
    private val client = WebClient.create("http://localhost:8010")

    @GetMapping("/reactive/{delayMillis}")
    fun get(@PathVariable delayMillis: String): Mono<String> {
        return client.get()
                .uri("/delay/$delayMillis")
                .retrieve()
                .bodyToMono(String::class.java)
                .map { s -> "Reactive:$s" }
    }
}

