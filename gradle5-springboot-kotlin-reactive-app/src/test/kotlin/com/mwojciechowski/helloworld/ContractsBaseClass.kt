package com.mwojciechowski.helloworld

import io.restassured.RestAssured
import io.restassured.module.mockmvc.RestAssuredMockMvc
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.boot.web.server.LocalServerPort



@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class ContractsBaseClass {

    @LocalServerPort
    var port: Int = 0

    @Before
    fun setup() {
        RestAssured.baseURI = "http://localhost"
        RestAssured.port = this.port
    }
}