package example

import io.micronaut.context.annotation.Property
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
@Property(name = "micronaut.server.port", value = "-1")
class HelloControllerTest {

    @Inject
    lateinit var helloClient: HelloClient

    @Test
    fun testGreetingService() {
        assertEquals(
                "Hello John",
                helloClient.hello("John").blockingGet()
        )
    }
}
