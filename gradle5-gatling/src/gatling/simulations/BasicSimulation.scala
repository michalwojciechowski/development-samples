import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, _}

import scala.concurrent.duration._

class BasicSimulation extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:8013")

  val scn = scenario("BasicSimulation").exec(
//    repeat(30) {
      exec(
//        http("request_1").get("/blocking/100")
          http("request_1").get("/reactive/100")
      )
//        .pause(1 second)
//    }
  )

  setUp(
    scn.inject(
//      rampUsers(500)
//        .during(5 seconds)

            incrementUsersPerSec(50)
              .times(15)
              .eachLevelLasting(1 seconds)
              .separatedByRampsLasting(1 second)
              .startingFrom(0)
    )
  ).protocols(httpProtocol)
    .assertions(global.successfulRequests.percent.is(100))
}