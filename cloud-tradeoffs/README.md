# Redhat & Openshift
Pros:
- Main choice for all the companies, no 1 for large clusters
- Openshift discounts are available
- Openshift routes & F5 integration is flawless

Side notes:
- Paid k8s support (e.g. [Pivotal K8s](https://pivotal.io/platform/pivotal-container-service)) is available as an alternative, although it doesn't mean it offers more than RedHat

![https://user-images.githubusercontent.com/300046/42333404-e3f5953a-8037-11e8-9691-0172a8a96388.jpg](k8s-vs-openshift.jpg)

# Azure & k8s

Side notes
- Azure default limits for your k8s cluster: https://docs.microsoft.com/en-us/azure/aks/quotas-skus-regions

# General cloud providers

Small business
- Public cloud

Medium business
- Private cloud

Large business
- Private cloud or on premise

Multi continental clusters & available products
- GCP: https://cloud.google.com/about/locations/?region=europe#region

Maturity
- varies from provider to provider, depends on the specific product, no silver bullet, experience & time required

# K8s setup
- storages: shared/nfs/replications/matrix
- infra: routing
- k8s agents & daemons
